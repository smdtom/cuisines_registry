package de.quandoo.recruitment.registry;

import de.quandoo.recruitment.registry.api.CuisinesRegistry;
import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class InMemoryCuisinesRegistry implements CuisinesRegistry
{
    private Map<Cuisine, Set<Customer>> cuisineCustomersMap = new ConcurrentHashMap<>();

    private Map<Customer, Set<Cuisine>> customerCuisinesMap = new ConcurrentHashMap<>();

    @Override
    public void register(final Customer customer, final Cuisine cuisine)
    {
        Set<Customer> customers = new HashSet<>();
        Set<Cuisine> cuisines = new HashSet<>();

        if (cuisineCustomersMap.containsKey(cuisine)) {
            customers = cuisineCustomersMap.get(cuisine);
        }

        customers.add(customer);
        cuisineCustomersMap.put(cuisine, customers);

        if (customerCuisinesMap.containsKey(customer)) {
            cuisines = customerCuisinesMap.get(customer);
        }

        cuisines.add(cuisine);
        customerCuisinesMap.put(customer, cuisines);
    }

    @Override
    public List<Customer> cuisineCustomers(final Cuisine cuisine)
    {
        if (cuisine == null) {
            return null;
        }

        Set<Customer> customers = cuisineCustomersMap.get(cuisine);

        return customers != null ? new ArrayList<>(customers) : null;
    }

    @Override
    public List<Cuisine> customerCuisines(final Customer customer)
    {
        if (customer == null) {
            return null;
        }

        Set<Cuisine>cuisines = customerCuisinesMap.get(customer);

        return cuisines != null ? new ArrayList<>(cuisines) : null;
    }

    @Override
    public List<Cuisine> topCuisines(final int n)
    {
        List<Cuisine> cuisines = cuisineCustomersMap.entrySet().stream()
            .sorted(Comparator.comparing(c -> c.getValue().size()))
            .map(Map.Entry::getKey)
            .collect(Collectors.toList())
        ;

        return cuisines.subList(Math.max(cuisines.size() - n, 0), cuisines.size());
    }
}
