package de.quandoo.recruitment.registry.model;

import java.util.UUID;

public class Customer
{
    private final UUID uuid;

    public Customer(final UUID uuid)
    {
        this.uuid = uuid;
    }

    public UUID getUuid()
    {
        return uuid;
    }
}
