package de.quandoo.recruitment.registry.model;

public class Cuisine 
{
    public enum Type
    {
        italian,
        french,
        german
    }
    
    private final Type type;

    public Cuisine(final Type type) 
    {
        this.type = type;
    }

    public Type getType()
    {
        return type;
    }
}
