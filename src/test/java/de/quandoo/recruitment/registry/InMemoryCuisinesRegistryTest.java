package de.quandoo.recruitment.registry;

import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;
import org.junit.Test;

import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class InMemoryCuisinesRegistryTest
{
    private InMemoryCuisinesRegistry cuisinesRegistry = new InMemoryCuisinesRegistry();

    @Test
    public void cuisineCustomers_should_return_expected_customers()
    {
        Cuisine cuisine = new Cuisine(Cuisine.Type.french);
        Customer customer = new Customer(UUID.randomUUID());

        cuisinesRegistry.register(customer, cuisine);

        assertEquals(Collections.singletonList(customer), cuisinesRegistry.cuisineCustomers(cuisine));
    }

    @Test
    public void cuisineCustomers_should_return_null_if_cuisine_is_null()
    {
        assertNull(cuisinesRegistry.cuisineCustomers(null));
    }

    @Test
    public void cuisineCustomers_should_return_null_if_cuisine_not_registered()
    {
        assertNull(cuisinesRegistry.cuisineCustomers(new Cuisine(Cuisine.Type.french)));
    }

    @Test
    public void customerCuisines_should_return_expected_customers()
    {
        Cuisine cuisine = new Cuisine(Cuisine.Type.french);
        Customer customer = new Customer(UUID.randomUUID());

        cuisinesRegistry.register(customer, cuisine);

        assertEquals(Collections.singletonList(cuisine), cuisinesRegistry.customerCuisines(customer));
    }

    @Test
    public void customerCuisines_should_return_null_if_customer_is_null() {
        assertNull(cuisinesRegistry.customerCuisines(null));
    }

    @Test
    public void customerCuisines_should_return_null_if_customer_not_registered()
    {
        assertNull(cuisinesRegistry.customerCuisines(new Customer(UUID.randomUUID())));
    }

    @Test
    public void topCuisines_should_return_expected_result()
    {
        Cuisine cuisineFrench = new Cuisine(Cuisine.Type.french);
        Cuisine cuisineItaly = new Cuisine(Cuisine.Type.italian);
        Customer customer1 = new Customer(UUID.randomUUID());
        Customer customer2 = new Customer(UUID.randomUUID());

        cuisinesRegistry.register(customer1, cuisineFrench);
        cuisinesRegistry.register(customer2, cuisineFrench);
        cuisinesRegistry.register(customer1, cuisineItaly);

        List<Cuisine> topCuisines = cuisinesRegistry.topCuisines(1);

        assertEquals(1, topCuisines.size());
        assertEquals(cuisineFrench.getType(), topCuisines.get(0).getType());
    }
}